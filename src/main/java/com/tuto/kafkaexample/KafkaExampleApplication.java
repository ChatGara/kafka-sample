package com.tuto.kafkaexample;

import com.tuto.kafkaexample.dto.Customer;
import com.tuto.kafkaexample.sender.ISender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaExampleApplication implements CommandLineRunner {

	@Autowired
	private ISender sender;

	public static void main(String[] args) {
		SpringApplication.run(KafkaExampleApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Customer customer1 = new Customer("Jean-Michel", "DUPONT");
		Customer customer2 = new Customer("Mathieu", "BLOIP");
		while (true) {
			sender.send("topicCustomer1", customer1);
			Thread.sleep(2000);
			sender.send("topicCustomer2", customer2);
			Thread.sleep(2000);
		}
	}
}
