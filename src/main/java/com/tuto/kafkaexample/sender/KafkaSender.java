package com.tuto.kafkaexample.sender;

import com.tuto.kafkaexample.dto.Customer;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaSender implements ISender {

    @Autowired
    private KafkaTemplate<String, Customer> kafkaTemplate;

    @Override
    public void send(String topicName, Customer customer) {
        System.out.println("[PRODUCER] : " + topicName + " : " + customer);
        new NewTopic(topicName, 1, (short) 1);
        kafkaTemplate.send(topicName, customer);
    }
}