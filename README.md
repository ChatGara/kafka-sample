# Kafka Example

## Process

* Install and launch docker service
* docker-compose up (start Kafka Container)
* Go To http://localhost:9000
* Add a new Cluster (Add Cluster)
  * Cluster Name : Your choice
  * Cluster Zookeeper Hosts : zookeeper:2181 (match data inside docker-compose.yaml)
* Initialize project (example : https://start.spring.io/ with Spring for Apache Kafka)
* Add dependency to jackson-databind (groupId : com.fasterxml.jackson.core)

Maven Example :
`
<dependency>
  <groupId>com.fasterxml.jackson.core</groupId>
  <artifactId>jackson-databind</artifactId>
</dependency>
`
* Launch App

## Docker

* A Dockerfile is present with a custom entry point in order to add a 20 seconds between launching Zookeeper and Kafka (using wurstmeister/kafka image), in order to avoid a Node already existing (Zookeeper have a 18 seconds of caches for brokers)

## KafkaManager

* Up at : http://localhost:9000

## Sources 

* https://dimbo.developpez.com/tutoriels/java/kafka/installation-kafka-et-execution-premier-programme-java/
* https://www.baeldung.com/spring-kafka