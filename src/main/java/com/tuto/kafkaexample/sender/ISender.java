package com.tuto.kafkaexample.sender;

import com.tuto.kafkaexample.dto.Customer;

public interface ISender {
    void send (String topicName, Customer customer);
}
