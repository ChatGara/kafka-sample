FROM wurstmeister/kafka

COPY ./entrypoint.sh /tmp/
RUN chmod a+x /tmp/entrypoint.sh

CMD ["/tmp/entrypoint.sh"]