package com.tuto.kafkaexample.receiver;

import com.tuto.kafkaexample.dto.Customer;

public interface IReceiver {

    public void listen (String topicName, Customer customer);
}
