package com.tuto.kafkaexample.receiver;

import com.tuto.kafkaexample.dto.Customer;
import org.springframework.stereotype.Service;

@Service
public class ProcessExample implements IProcess {
    public void execute(String info, Customer customer) {
        System.out.println("[RECEIVER] : " + info + " : " + customer);
    }
}
