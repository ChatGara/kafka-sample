package com.tuto.kafkaexample.receiver;

import com.tuto.kafkaexample.dto.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaReceiver implements IReceiver {
    @Autowired
    private IProcess process;

    @KafkaListener(topics = "topicCustomer1", containerFactory = "kafkaListenerContainerFactory")
    public void listenCustomer1(Customer customer) {
        listen("topicCustomer1", customer);
    }

    @KafkaListener(topics = "topicCustomer2", containerFactory = "kafkaListenerContainerFactory")
    public void listenCustomer2(Customer customer) {
        listen("topicCustomer2", customer);
    }

    public void listen(String topicName, Customer customer) {
        process.execute(topicName, customer);
    }
}
