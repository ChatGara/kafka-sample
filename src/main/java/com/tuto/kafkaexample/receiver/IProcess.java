package com.tuto.kafkaexample.receiver;

import com.tuto.kafkaexample.dto.Customer;

public interface IProcess {

    public void execute(String info, Customer customer);
}
